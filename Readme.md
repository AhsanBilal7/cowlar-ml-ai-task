# Table of Content
- [Task 1: Car Price Prediction](#car-price-prediction)
- [Task 2: Movie Recommendation System](#movie-recommendation-system)
# Car Price Prediction

## Dataset Summary

The dataset, named "cars_data.csv," contains information about various cars, including their make, model, version, price, make year, engine capacity (CC), assembly, mileage, registered city, and transmission.

## My Approach

- Develop a machine learning model that accurately predicts the price of a car based on its features. 
- Machine Learning Model is trained on the feature.
- Implementation of Data cleaning/ feature engineering to optimize the model
- Apply the PCA, but no fitting well on the required problem
- Applied different Regression problems, As there is non-linearity in the dataset so simple linear regression failed on this problem
- Applied the MLP Regression and Rnadom Forest algorithm so to cater the non-linearity in the dataset or to be precise in the requried problem

## Key Insights

The dataset consists of the following columns:

- **Make:** The make of the car (e.g., Honda, Mitsubishi, Audi, Toyota).
- **Model:** The model of the car (e.g., Insight, Minica, A6, Aqua).
- **Version:** The specific version or edition of the car.
- **Price:** The price of the car in the local currency.
- **Make_Year:** The year of manufacture for the car (Can be used to calculate how old the car is).
- **CC:** The engine capacity of the car.
- **Assembly:** Whether the car is locally assembled or imported.
- **Mileage:** The mileage or distance traveled by the car.
- **Registered City:** The city where the car is registered.
- **Transmission:** The type of transmission used in the car.

### Model Development
I have applied 
- Random Forest Regression  `MSE: 0.026743607117710223`
- MultiLayer Perceptron (MLP) Regression `MSE: 0.13870359144597597`
- SDGRegression  `MSE: 2.989963287169141e+24`
<br>**Random Forest Regression is performing good on this problem**

1. **Data Preprocessing:** Handle missing values, encode categorical variables, and scale numerical features to using Standard Scalar techniques.
2. **Regression Algorithm:** Random Forest Regression

### Difficulties

- Some entries don't have the respective labels
- Encoding categorical variables to numerical values to feed to the model


--------------------------------------------------------------------------------

# Movie Recommendation System

## Dataset Summary
The dataset consists of user ratings for various movies. Each row represents a movie, and the columns represent user ratings on a scale of 0 to 5. The dataset is in the form of a user-item matrix, where rows correspond to movies and columns correspond to user ratings.

## My Approach
Understand what is the problem and what type of dataset we have. I have implemented a movie recommendation system using a correlation matrix as only user rating and simple movie title is given. The system suggests movies to users based on the correlation between their preferences and those of other users. The higher the correlation between two movies, the more likely they are to be recommended together.

## Key Components
1. **Correlation Matrix**
I calculated the correlation between movies using user ratings. The correlation matrix helps identify movies that are similar in terms of user preferences.

2. **Movie Recommendation Function**
I developed a function that takes a movie title as input and returns a list of recommended movies based on the correlation matrix. The function considers the users' past ratings to provide personalized recommendations.

## Implementation
I followed these steps to implement the recommendation system:

**Data Loading**: Loaded the movie ratings dataset, where each row represents a movie and each column represents a user's ratings.

**Correlation Calculation / Cosine Similarity**: Calculated the correlation matrix between movies using user ratings. This matrix captures the similarity between movies.

**Recommendation Function**: Created a function that takes a movie title input and returns a list of recommended movies based on the correlation matrix.

## Difficulty
- Problem is pretty much simple and data is also provided with no null entries





